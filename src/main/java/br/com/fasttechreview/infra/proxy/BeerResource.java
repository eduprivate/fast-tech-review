package br.com.fasttechreview.infra.proxy;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/v2")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BeerResource {

   @GET
   @Path("/beers/{id}")
   public String getBeer(@PathParam("id") long id);

   @GET
   @Path("/beers")
   public String getBeers(@QueryParam("params") String params);

}