package br.com.fasttechreview.infra.proxy;

import java.util.Collections;
import java.util.Map;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.fasttechreview.infra.metrics.BeerQueryProfiler;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ProxyBeerService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProxyBeerService.class);
	private BeerQueryProfiler beerQueryProfiler;
	
	@Autowired
	public ProxyBeerService(BeerQueryProfiler beerQueryProfiler) {
		this.beerQueryProfiler = beerQueryProfiler;
	}
	
	@Cacheable("beer")
	@HystrixCommand(commandKey="getBeer" ,fallbackMethod = "fallbackProxyGetBeer")
	public String getBeer(long beerId) {
		beerQueryProfiler.requestQuery();
		LOGGER.info("Params {}", beerId);
		BeerResource proxy = getProxyClient();
		String beers = proxy.getBeer(beerId);
		return beers;
	}
	
	public String fallbackProxyGetBeer(long beerId) {
		LOGGER.info("Fallback getBeer {}", this.getClass());
		return Collections.EMPTY_LIST.toString();
	}
	
	@Cacheable("beers")
	@HystrixCommand(commandKey="findBeers" ,fallbackMethod = "fallbackProxyFindBeers")
	public String findBeers(Map<String, String> params) {
		beerQueryProfiler.requestQuery();
		
		BeerResource proxy = getProxyClient();
		String stringParams = getUrlParameters(params);
		LOGGER.info("Params {}", stringParams);
		
		String beers = proxy.getBeers(stringParams);
		return beers.toString();
	}
	
	public String fallbackProxyFindBeers(Map<String, String> params) {
		LOGGER.info("Fallback findBeer {}", this.getClass());
		return Collections.EMPTY_LIST.toString();
	}
	
	private String getUrlParameters(Map<String, String> params) {
		StringBuilder paramsBuilders = new StringBuilder();
		if(params != null && !params.isEmpty()){
			paramsBuilders.append("?");
			for (Map.Entry<String, String> entry : params.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
			    paramsBuilders.append("&"+key+"="+value);
			}
		}
		return paramsBuilders.toString();
	}
	
	private BeerResource getProxyClient() {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target("https://api.punkapi.com");
		BeerResource proxy = target.proxy(BeerResource.class);
		return proxy;
	}
}