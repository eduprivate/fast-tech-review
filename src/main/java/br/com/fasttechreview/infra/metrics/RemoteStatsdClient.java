package br.com.fasttechreview.infra.metrics;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteStatsdClient implements StatsdClient {

	private static Logger LOGGER = LoggerFactory.getLogger(RemoteStatsdClient.class.getName());

	private final InetSocketAddress address;
	private DatagramChannel channel;

	public RemoteStatsdClient(String host, int port) throws UnknownHostException, IOException {
		this(InetAddress.getByName(host), port);
	}

	public RemoteStatsdClient(InetAddress host, int port) throws IOException {
		address = new InetSocketAddress(host, port);
		channel = DatagramChannel.open();
	}

	@Override
	public boolean timing(String key, long value) {
		return send(String.format("%s:%d|ms", key, value));
	}

	@Override
	public boolean increment(String key) {
		return send(String.format("%s:%s|c", key, 1));
	}

	@Override
	public boolean increment(String key, int magnitude) {
		return send(String.format("%s:%s|c", key, magnitude));
	}

	@Override
	public boolean sets(String key, long value) {
		return send(String.format("%s:%s|s", key, value));
	}

	@Override
	public boolean gauges(String key, long value) {
		return send(String.format("%s:%s|g", key, value));
	}

	private boolean send(final String stat) {

		String hostName = address.getHostName();
		int port = address.getPort();
		try {
			final byte[] data = stat.getBytes("utf-8");
			if (channel.isOpen()) {
				final int nbSentBytes = channel.send(ByteBuffer.wrap(data),
						address);

				if (data.length == nbSentBytes) {
					return true;

				} else {
					LOGGER.error(String
							.format("Could not send entirely stat %s to host %s:%d. Only sent %i bytes out of %i bytes",
									stat, hostName, port, nbSentBytes,
									data.length));

				}
			} else {
				LOGGER.error("Statsd socket is closed! Droped metric: {}", stat);
				channel = DatagramChannel.open();
			}
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(String.format("Error generating StatsD Payload. Couldn't generate byte array"));
		} catch (IOException e) {
			LOGGER.error(String.format("Could not send stat %s to host %s:%d", stat, hostName, port), e);
			return false;
		}

		return false;
	}

}