package br.com.fasttechreview.infra.metrics;

public interface StatsdClient {

	boolean increment(String key);

	boolean increment(String key, int magnitude);

	boolean timing(String key, long timeInMilliseconds);

	boolean gauges(String key, long value);

	boolean sets(String key, long value);
}