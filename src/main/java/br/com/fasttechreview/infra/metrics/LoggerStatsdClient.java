package br.com.fasttechreview.infra.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class LoggerStatsdClient implements StatsdClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerStatsdClient.class);

	@Override
	public boolean timing(String key, long timeInMilliseconds) {
		LOGGER.debug("timing.{}:{}|ms", key, timeInMilliseconds);
		return true;
	}

	@Override
	public boolean sets(String key, long value) {
		LOGGER.debug("sets.{}:{}|s", key, value);
		return true;
	}

	@Override
	public boolean increment(String key, int magnitude) {
		LOGGER.debug("increment.{}:1|c|@{}", key, magnitude);
		return true;
	}

	@Override
	public boolean increment(String key) {
		LOGGER.debug("increment.{}:1|c", key);
		return true;
	}

	@Override
	public boolean gauges(String key, long value) {
		LOGGER.debug("gauge.{}:{}|g", key, value);
		return true;
	}
}
