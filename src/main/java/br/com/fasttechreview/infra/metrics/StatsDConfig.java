package br.com.fasttechreview.infra.metrics;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StatsDConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(StatsDConfig.class);

	@Value("${STATSD_ADDRESS}")
	private String stastdAddr;
	@Value("${STATSD_PORT:8125}")
	private int statsDPort;

	@Bean
	@ConditionalOnProperty(name = "statsd.enabled", havingValue = "true", matchIfMissing = true)
	public StatsdClient createStatsdClient() {
		try {
			return new RemoteStatsdClient(stastdAddr, statsDPort);
		} catch (IOException e) {
			LOGGER.error("Error while connecting to StatsD", e);
		}
		return null;
	}

	@Bean
	@ConditionalOnProperty(name = "statsd.enabled", havingValue = "false", matchIfMissing = false)
	StatsdClient loggerStatsD() {
		return new LoggerStatsdClient();
	}

}