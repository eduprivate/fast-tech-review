package br.com.fasttechreview.infra.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class StatsD {

	private static final Logger LOGGER = LoggerFactory.getLogger(StatsD.class);

	private final StatsdClient statsdClient;
	private final StatsdKeyBuilder fastTechBaseStatsdKey;

	@Autowired
	public StatsD(StatsdClient statsdClient, @Value("${STATSD_ENV}") String fastTechEnvironment)  {
		this.statsdClient = statsdClient;
		this.fastTechBaseStatsdKey = new StatsdKeyBuilder(fastTechEnvironment).addPrefix("search.frenzy");
	}

	@Async
	public void timing(String value, long milliseconds) {
		LOGGER.trace("StatsD.timing({}, {})", value, milliseconds);
		String timingKey = fastTechBaseStatsdKey.build(value);
		statsdClient.timing(timingKey, milliseconds);
	}
	@Async
	public void increment(String key) {
		LOGGER.trace("StatsD.increment({})", key);

		String incrementKey = fastTechBaseStatsdKey.build(key);
		statsdClient.increment(incrementKey);
	}

}