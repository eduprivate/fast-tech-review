package br.com.fasttechreview.infra.metrics;

public enum Metric {
	
	REQUEST_QUERY("beer.request.query"),
	REQUEST_ERROR("beer.request.error");

	private String statsdKey;

	private Metric(String statsD) {
		this.statsdKey = statsD;
	}

	public String getStatsDKey() {
		return statsdKey;
	}

	public String getParametrizedStatsDKey(Object ...params) {
		return String.format(statsdKey, params);
	}

	public String getStatsDKey(String keyToBeAppended) {
		if (keyToBeAppended != null) {
			return statsdKey + "." + keyToBeAppended;
		}
		return getStatsDKey();
	}
}