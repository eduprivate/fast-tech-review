package br.com.fasttechreview.infra.metrics;

import java.util.regex.Pattern;

public class StatsdKeyBuilder {

	private static final Pattern STATSD_VALID_KEY_PATTERN = Pattern.compile("^(\\w+\\.)*(\\w+)$");
	private static final char SEPARATOR = '.';

	private final String prefix;

	public StatsdKeyBuilder(String prefix) {
		validateKeyString(prefix);
		this.prefix = prefix;
	}

	private void validateKeyString(String key) {
		if (!STATSD_VALID_KEY_PATTERN.matcher(key).matches()) {
			throw new IllegalArgumentException(key + "is not a valid only word/digit/dot string without ending with dot."
					+ "(Example: \"key\" or \"prefix.prefix2\")");
		}
	}

	public StatsdKeyBuilder addPrefix(String morePrefix) {
		validateKeyString(morePrefix);
		return new StatsdKeyBuilder(prefix + SEPARATOR + morePrefix);
	}

	public String build(String childKey) {
		validateKeyString(childKey);
		return new StringBuilder(prefix).append(SEPARATOR).append(childKey).toString();
	}
}