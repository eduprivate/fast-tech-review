package br.com.fasttechreview.infra.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BeerQueryProfiler {

	private final StatsD statsD;

	@Autowired
	public BeerQueryProfiler(StatsD statsD) {
		this.statsD = statsD;
	}

	public void requestQuery(){
		statsD.increment(Metric.REQUEST_QUERY.getStatsDKey());
	}

	public void requestError(){
		statsD.increment(Metric.REQUEST_ERROR.getStatsDKey());
	}
}