package br.com.fasttechreview.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fasttechreview.infra.proxy.ProxyBeerService;

@RestController
public class BeerController {

	private ProxyBeerService proxyBeerService;
	
	@Autowired
	public BeerController(ProxyBeerService proxyBeerService) {
		this.proxyBeerService = proxyBeerService;
	}
	
	@RequestMapping(value="/v2/beers/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> proxyBeer(@PathVariable(value = "id", required=true) long id) {
		return new ResponseEntity<String>(proxyBeerService.getBeer(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="/v2/beers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> proxyBeers(@RequestParam Map<String, String> params) {
		return new ResponseEntity<String>(proxyBeerService.findBeers(params), HttpStatus.OK);
	}
}