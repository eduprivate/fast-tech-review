# Fast Tech Review

### Disclamier

Você deverá fazer todas as etapas no menor tempo possível, nossa expectativa entre seu aceite e a última fase é de 3 dias.

### Roadmap

1. Vamos bater um papo rápido, pode ser presencial ou por skype
2. Crie um fork desse repositório, e clone-o
3. Responda os itens #1, #2 e #3 do Tech Review abaixo
4. Gere um ZIP do seu repositório e envie para cristian.medeiros@tricae.com.br. No assunto coloque "Fast Tech Review - `<github username>`". Pedimos nesse formato para manter esse repositório sem histórico de respostas.
5. In office coding time - Após enviar seu teste, dá um ping no skype/e-mail para agendarmos sua visita, você passará umas 2hs~2h:30 aqui conosco. A idéia é vermos uma implementação de algum algoritmo simples, sem white board.

### Tech Review

1. Desenhe duas alternativas de armazenamento de dados para um sistema de Catalogo de Cervejas, descreva suas VANTAGENS e DESVANTAGENS. Pense em um cenário de alta concorrência, tipo um e-commerce :) (escrita moderada, leitura pesada). Poderão ser estruturas diferentes de dados ou até bancos de dados diferentes (Apenas precisam ser soluções open source).

**Exemplo de resposta de uma consulta que seu modelo deverá retornar** https://api.punkapi.com/v2/beers/25

**Como apresentar sua solução** Mostre seu desenho do banco: pode ser um arquivo SQL, um schema JSON ou um arquivo texto explicando sua abordagem

2. [API Proxy](https://www.quora.com/What-is-API-Proxy) 

- Queremos entender como você aborda as falhas que uma API possa gerar. 
Para isso: 
- Faça um API Proxy para esta interface [https://api.punkapi.com/v2/beers](https://api.punkapi.com/v2/beers) - [Documentação](https://punkapi.com/documentation/v2)
    * Aplique os filtros: abv_gt, abv_lt, ibu_gt, ibu_lt, ebc_gt, ebc_lt, beer_name
    * Faça entre 1 e 3 requisições por segundo, para alcançar algumas limitações documentadas
    * Não deverá ser possível listar todos os itens em uma única request

**Como apresentar sua solução:** Escreva o código na linguagem que prefir, pode usar framework, pode ser pseudo-code

3. Escreva um pouco sobre os tópicos [deste](questions.md) link e envie junto com suas respostas. Pode ser um por assunto, um por linha ou uma redação.


# Soluções e Respostas

Problema 1. 

## Solução 1
A primeira solução apresenta uma modelagem usando MySQL e é em cima desta modelagem que foi escrito o aplicativo de exemplo (commit anterior, master esta simplificado). A modelagem pode ser vista em 'scripts/create.sql'
VANTAGENS: A estrutura de dados fica normalizada e não há duplifidade de informação. Se houver aumento de escrita na base podemos criar uma estrutura Master/Slave. Para cenários de alta leitura caches de primeiro nível e de segundo nível podem ser utilizados e melhoram o tempo de resposta consideravelmente.
DESVANTAGENS: Querys são SQL Like, buscas por beer_name deve ter match exato ou usamos o 'constains' do SQL o que torna as querys mais lentas. Devemos ter cuidado nas querys com relacionamentos para evitar 'full table scan'. A criação de 'constrainsts' geralmente ajuda a evitar isso. 

## Solução 2
A segunda solução usa uma base 'NoSQL' Solr. A modelagem pode ser vista em 'scripts/schema.xml'
VANTAGENS: Querys usando indices Solr costumas ser eficientes além de possibilitarem buscas textuais. O Solr possui seus próprios caches que, se bem configurados, aumentam a eficiencia das querys.
DESVANTAGENS: Como o Solr trabalha com documentos os dados entre eles podem ficar duplicados. Outro problema também séria o tunning do cache do Solr. Como o cache fica na JVM podem ocorrer ciclos de GC que aumentam o uso de CPU e consequentemente aumentam o tempo de resposta. Recomenda-se não usar um cache grande e configurar o warmup do Solr com querys de 'long tail' (querys profundas). 


Problema 2. 

# Solução
Criei uma aplicação usando spring-boot. Para fazer o proxy do serviço utilizei o Resteasy. A api 'https://api.punkapi.com' apresenta uma latência consideravelmente grande (em torno de 1s ou mais) por isso utilizei um cache (ehcache, configuração padrão) e para tratar de latencias muitos grandes ou falhas na chamada da api utilizei um Circuit-Break com Hystrix configurado com 100 threads (para requests de até 3 req/sec - Little's Law) e um timeout de até 1000 ms. Se o conteúdo não estiver cacheado e houver problemas com a query o fallback retorna uma busca vazia.
Para realizar os requests utilizei o [Vegeta](https://github.com/tsenart/vegeta).
Possíveis melhorias: configuração do cache (tamanho, ttl, estratégias de evictions, etc) para melhor performance (hit/miss). 
Fallback poderia utilizar serviço interno ou resposta curada.

## Testando a aplicação
`cp local.env.sample local.env`

`$./scripts/startLocal`

ou 

`mvn clean package docker:build`

## Testes

## warmup
`echo "GET http://localhost:8000/v2/beers?beer_name=Buzz&abv_lt=5&abv_gt=1&ibu_gt=50&ibu_lt=80&ebc_gt=10&ebc_lt=50" | vegeta attack -rate=50 -duration=5s |  vegeta report`

## attack
`echo "GET http://localhost:8000/v2/beers?beer_name=Buzz&abv_lt=5&abv_gt=1&ibu_gt=50&ibu_lt=80&ebc_gt=10&ebc_lt=50" | vegeta attack -rate=4000 -duration=5s |  vegeta report`


# Questions

## Answers

### Comportamento
- Como admitir erros e aceitar críticas sem levar para o lado pessoal.

**R:  Aceitar críticas e reconhecer erros é umas das melhores formas de crescimento pessoal e profissional. Não importa de quem venha e são sempre bens vindas.** 

- Como calçar os sapatos do usuário, e tentar entender sua visão.

**R: Entender que o usuário é quem melhor deve estar satisfeito no negócio. Entender suas dificuldades e problemas, seja conversando, pesquisas, tracking de comportamento ou mesmo 'sendo' o usuário.**

- Quando pedir ajudar, quando não pedir ajuda.

**R: Acredito que o profissional deve pedir ajuda quando não sabe ou entende o que precisa fazer ou qual a melhor forma de faze-lo ou, mesmo que saiba, esta tendo muita dificuldade e levando mais tempo que uma pessoa com mais experiência tomaria para fazer o mesmo.** 

- Como saber que você está fazendo, naquele momento, a coisa mais importante que deveria estar fazendo.

**R: Seja seguindo o que foi priorizado pela equipe, atuando em algum problema que precisa ser resolvido ou realizando uma entrega de valor (para o usuário ou empresa).** 


### Código
- Como ler o código de outras pessoas

**R: Pessoas são diferentes, pensam diferentes e tem estilos de código diferentes. Devemos respeitar essas diferenças e fazer uma análise mais objetiva do código como por exemplo o uso de boas práticas, convenções já estabelecidas, corretude de código e análise de algoritmo (tempo, uso de memória, cpu), cobertura de testes.** 

- Descreva o SOLID com suas proprias palavras.

**R: SOLID são princípios de projetos Orientados a Objetos de visam orientar um bom design de código.** 

- Como você sabe que está escrevendo a coisa certa?

**R: Escrevendo código que resolve o problema de maneira mais simples o possível de maneira a evitar desperdício de recursos (mem, cpu, mais infra) sem aumentar (ou tentar) a complexidade do sistema.**

- Como você sabe que está escrevendo certo?

**R: Tento prestar atenção na corretude de código, convenções e boas práticas e TDD.** 

### Pessoal
- Como você estuda/consome informação. Descreva suas fontes, qual é o seu processo e com que frequencia.

**R: Estudo quase que diariamente. Leio livros, sites, blogs e aprendo muito no dia a dia com colegas. Nesses estudos aprendo tecnologias novas (ou linguagens, frameworks, técnicas), coisas que acredito que terão uso no dia a dia, como um projeto que estou trabalhando e de quando em quando reviso muita coisa que já estudei, o que chamo de afiar o machado. **

- Qual é o seu hobby?  

**R: Ler livros (tenho minha própria biblioteca), cozinhar e ficar com a família. Codar.**