CREATE SCHEMA fasttechreview;

USE fasttechreview;

CREATE TABLE beer (
	id					INT NOT NULL AUTO_INCREMENT,
	beer_name			VARCHAR(255),
	tagline				VARCHAR(255),
	first_brewed		DATE,
	description			VARCHAR(500),
	image_url			VARCHAR(255),
	abv					DOUBLE,
	ibu					DOUBLE,
	target_fg			INT,
	target_og			INT,
	ebc					INT,
	srm					INT,
	ph					DOUBLE,
	attenuation_level	INT,
	brewers_tips		VARCHAR(255),
	contributed_by		VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE volume (
	id				INT NOT NULL AUTO_INCREMENT,
	volume_value	DOUBLE NOT NULL,
	volume_unit		VARCHAR(100),
	beer_id			INT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);

CREATE TABLE boil_volume (
	id				INT NOT NULL AUTO_INCREMENT,
	volume_value	DOUBLE NOT NULL,
	volume_unit		VARCHAR(100),
	beer_id				INT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);

CREATE TABLE method (
	id				INT NOT NULL AUTO_INCREMENT,
	method_name		VARCHAR(255),
	temp_value		DOUBLE NOT NULL,
	temp_unit		VARCHAR(100),
	beer_id			INT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);

CREATE TABLE ingredient (
	id					INT NOT NULL AUTO_INCREMENT,
	ingredient_name		VARCHAR(255),
	amount_value		DOUBLE NOT NULL,
	amount_unit			VARCHAR(100),
    beer_id				INT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);

CREATE TABLE food_pairing (
	id					INT NOT NULL AUTO_INCREMENT,
	food				VARCHAR(255),
    beer_id				INT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);

CREATE TABLE hops (
	id					INT NOT NULL AUTO_INCREMENT,
	hops_name				VARCHAR(255),
	hops_unit				VARCHAR(255),
    hops_value				DOUBLE,
	beer_id				INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (beer_id) REFERENCES beer(id)
);


INSERT INTO `fasttechreview`.`beer` (`id`, `abv`, `attenuation_level`, `brewers_tips`, `contributed_by`, `description`, `ebc`,  `ibu`, `image_url`, `beer_name`, `ph`, `srm`, `tagline`, `target_fg`, `target_og`) VALUES ('1', '4.7', '79', 'Make sure you have plent…ly during fermentation.', 'Sam Mason <samjbmason>', '2008 Prototype beer, a 4…erries and citrus peel.', '8', '45', 'https://images.punkapi.com/v2/25.png', 'Bad Pixie', '4.4', '4', 'Spiced Wheat Beer.', '1010', '1047');

INSERT INTO `fasttechreview`.`boil_volume` (`id`, `volume_value`, `volume_unit`, `beer_id`) VALUES ('1', '25', 'liters', '1');

INSERT INTO `fasttechreview`.`food_pairing` (`id`, `food`, `beer_id`) VALUES ('1', 'Poached sole fillet with capers', '1');
INSERT INTO `fasttechreview`.`food_pairing` (`id`, `food`, `beer_id`) VALUES ('2', 'Summer fruit salad', '1');
INSERT INTO `fasttechreview`.`food_pairing` (`id`, `food`, `beer_id`) VALUES ('3', 'Banana split', '1');

INSERT INTO `fasttechreview`.`ingredient` (`id`, `ingredient_name`, `amount_value`, `amount_unit`, `beer_id`) VALUES ('1', 'Wheat', '2.5', 'kilograms', '1');
INSERT INTO `fasttechreview`.`ingredient` (`id`, `ingredient_name`, `amount_value`, `amount_unit`, `beer_id`) VALUES ('2', 'Extra Pale', '2.06', 'kilograms', '1');

INSERT INTO `fasttechreview`.`method` (`id`, `method_name`, `temp_value`, `temp_unit`, `beer_id`) VALUES ('1', 'mash_temp', '67', 'celsius', '1');
INSERT INTO `fasttechreview`.`method` (`id`, `method_name`, `temp_value`, `temp_unit`, `beer_id`) VALUES ('2', 'fermentation', '19', 'celsius', '1');

INSERT INTO `fasttechreview`.`volume` (`id`, `volume_value`, `volume_unit`, `beer_id`) VALUES ('1', '20', 'liters', '1');